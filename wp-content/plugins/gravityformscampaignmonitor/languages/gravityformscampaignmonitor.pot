# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Gravity Forms Campaign Monitor Add-On\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-22 16:48-0500\n"
"PO-Revision-Date: 2014-10-22 16:49-0500\n"
"Last-Translator: Dana Cobb <dana@rocketgenius.com>\n"
"Language-Team: Rocketgenius <customerservice@rocketgenius.com>\n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 1.6.10\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: _e;__\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SearchPath-0: ..\n"

#: ../class-gf-campaignmonitor.php:40
msgid "Subscribe user to Campaign Monitor only when payment is received."
msgstr ""

#: ../class-gf-campaignmonitor.php:49
msgid "Campaign Monitor Account Information"
msgstr ""

#: ../class-gf-campaignmonitor.php:50
#, php-format
msgid ""
"Campaign Monitor is an email marketing software for designers and their "
"clients. Use Gravity Forms to collect customer information and automatically "
"add them to your client's Campaign Monitor subscription list. If you don't "
"have a Campaign Monitor account, you can  %1$ssign up for one here%2$s"
msgstr ""

#: ../class-gf-campaignmonitor.php:55
msgid "API Key"
msgstr ""

#: ../class-gf-campaignmonitor.php:63
msgid "API Client ID"
msgstr ""

#: ../class-gf-campaignmonitor.php:79
msgid ""
"You can find your unique API key by clicking on the 'Account Settings' link "
"at the top of your Campaign Monitor screen."
msgstr ""

#: ../class-gf-campaignmonitor.php:95
msgid ""
"(Optional) Enter an API Client ID to limit this Add-On to the specified "
"client."
msgstr ""

#: ../class-gf-campaignmonitor.php:111
#, php-format
msgid ""
"We are unable to login to Campaign Monitor with the provided API key. Please "
"make sure you have entered a valid API key in the %sSettings Page%s"
msgstr ""

#: ../class-gf-campaignmonitor.php:127
msgid "Campaign Monitor Feed"
msgstr ""

#: ../class-gf-campaignmonitor.php:132 ../class-gf-campaignmonitor.php:136
#: ../class-gf-campaignmonitor.php:383
msgid "Name"
msgstr ""

#: ../class-gf-campaignmonitor.php:136
msgid "Enter a feed name to uniquely identify this setup."
msgstr ""

#: ../class-gf-campaignmonitor.php:140 ../class-gf-campaignmonitor.php:145
msgid "Client"
msgstr ""

#: ../class-gf-campaignmonitor.php:145
msgid ""
"Select the Campaign Monitor client you would like to add your contacts to."
msgstr ""

#: ../class-gf-campaignmonitor.php:149 ../class-gf-campaignmonitor.php:153
msgid "Contact List"
msgstr ""

#: ../class-gf-campaignmonitor.php:153
msgid ""
"Select the Campaign Monitor list you would like to add your contacts to."
msgstr ""

#: ../class-gf-campaignmonitor.php:157 ../class-gf-campaignmonitor.php:160
msgid "Map Fields"
msgstr ""

#: ../class-gf-campaignmonitor.php:160
msgid ""
"Associate your Campaign Monitor custom fields to the appropriate Gravity "
"Form fields by selecting the appropriate form field from the list."
msgstr ""

#: ../class-gf-campaignmonitor.php:164
msgid "Opt In"
msgstr ""

#: ../class-gf-campaignmonitor.php:167
msgid "Opt-In Condition"
msgstr ""

#: ../class-gf-campaignmonitor.php:167
msgid ""
"When the opt-in condition is enabled, form submissions will only be exported "
"to Campaign Monitor when the condition is met. When disabled all form "
"submissions will be exported."
msgstr ""

#: ../class-gf-campaignmonitor.php:171
msgid "Options"
msgstr ""

#: ../class-gf-campaignmonitor.php:253 ../class-gf-campaignmonitor.php:260
msgid "Resubscribe"
msgstr ""

#: ../class-gf-campaignmonitor.php:260
msgid ""
"When this option is enabled, if the subscriber is in an inactive state or "
"has previously been unsubscribed, they will be re-added to the active list. "
"Therefore, this option should be used with caution and only when appropriate."
msgstr ""

#: ../class-gf-campaignmonitor.php:266
msgid ""
"This option will re-subscribe users that have been unsubscribed. Use with "
"caution and only when appropriate."
msgstr ""

#: ../class-gf-campaignmonitor.php:384
msgid "Campaign Monitor Client"
msgstr ""

#: ../class-gf-campaignmonitor.php:385
msgid "Campaign Monitor List"
msgstr ""

#: ../class-gf-campaignmonitor.php:413 ../class-gf-campaignmonitor.php:436
msgid "(List not found in Campaign Monitor"
msgstr ""
