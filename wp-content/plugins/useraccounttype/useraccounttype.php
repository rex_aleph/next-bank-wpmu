<?php
/**
 * @package User Account Type
 * @version 1.0
 */
/*
Plugin Name: User Account Type
Description: Adds user account type to user profile
Author: Aleph Labs
Version: 1.0
*/

add_action('show_user_profile', 'userAccountTypeAdd');
add_action('personal_options_update', 'userAccountTypeAddUpdate');

function userAccountTypeAdd(){ 
    global $user_ID;
    $account_add = get_user_meta($user_ID, "account_type");
    if(is_array($account_add))
        $account_add = $account_add[0];
    ?>
    <h3>Extra Fields</h3>
    <table class="form-table">
			<tr>
				<th><label for="account_type">Account Type</label></th>
				<td>
					<select name="account_type" id="account_type">
					<?php
						$account_type = array();						
						$account_type['individual']  = "Individual";
						$account_type['startup']  = "Startup";
						$account_type['corporate']  = "Corporate";						
						$account_type = array_map( 'trim', $account_type );
						$account_type = array_unique( $account_type );
						foreach ( $account_type as $id => $item ) {
					?>
						<option id="<?php echo $id; ?>" value="<?php echo esc_attr($item); ?>"<?php selected( $account_add, $item ); ?>><?php echo $item; ?></option>
					<?php
						}
					?>						
					</select>
				</td>
				<span class="description">Please select user account type.</span></td> 				
			</tr>					
    </table>
    <?php            
        }

function userAccountTypeAddUpdate(){
    global $user_ID;
    update_user_meta($user_ID, "account_type",$_POST['account_type']);   
}
?>
