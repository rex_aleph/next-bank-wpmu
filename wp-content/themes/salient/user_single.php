<?php
/*
Template Name: User Profile Single
*/
get_header(); 
function get_user($id) {	
	$user = get_userdata($id);
	return $user;
}

?>

<div class="container-wrap">
	
	<div class="container main-content">
		
		<div class="row">
						
	<?php
	$user_info = get_user($_GET['user_id']);
	
	if (!empty($user_info) && !empty($_GET['user_id']))
	{                
		echo '<div class="user-profile">';
		if (function_exists('get_avatar')) { echo '<div class="user-avatar">';echo get_avatar( $user_info->user_email, 190 );echo "</div>"; }
		echo "<p>";
		echo '<b>Name:</b> '.$user_info->display_name;
		echo "<br>";
		$title = get_user_meta($user_info->ID, 'title', true);
		if(!empty($title)) {
			echo '<b>Title:</b> '.$title;
			echo "<br>";			
		}		
		$description = get_user_meta($user_info->ID, 'description', true);
		if(!empty($description)) {
			echo '<b>Description:</b> '.$description;
			echo "<br>";			
		}
		$phone = get_user_meta($user_info->ID, 'phone', true);
		if(!empty($phone)) {
			echo '<b>Phone:</b> '.$phone;
			echo "<br>";			
		}
		$address = get_user_meta($user_info->ID, 'address', true);
		if(!empty($address)) {
			echo '<b>Address:</b> '.$address;
			echo "<br>";			
		}
		if(!empty($user_info->user_url)) {
			echo '<b>Web site:</b> <a href="'.$user_info->user_url.'" target="_blank">'.$user_info->user_url.'</a>';		
			echo "<br>";			
		}
		$twitter = get_user_meta($user_info->ID, 'twitter', true);
		if(!empty($twitter)) {
			echo '<b>Twitter:</b> '.$twitter;
			echo "<br>";			
		}	
		echo "</p>";
		echo "</div>";
	} else {
		echo 'User not found';
	}
        echo '<div class="linkToUserList"><a href="'.site_url().'/community">Back to Community</a></div>';        
	?>
	
		</div><!--/row-->
		
	</div><!--/container-->
	
</div>
<?php get_footer(); ?>