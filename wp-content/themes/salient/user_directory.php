<?php get_header(); ?>

<?php 

$bg = get_post_meta($post->ID, '_nectar_header_bg', true);
$bg_color = get_post_meta($post->ID, '_nectar_header_bg_color', true);




?>

<?php


/*
Template Name: User Directory
*/

define("USERPROFILE_OUTLINE", false);
define("USERPROFILE_CITY", true);
define("LINKEDIN_ONLY", true);

get_header(); 



function get_clients() { 

    $users = array();
    $roles = array('subscriber', 'administrator');

    foreach ($roles as $role) :
        $users_query = new WP_User_Query( array( 
            'fields' => 'all_with_meta', 
            'role' => $role, 
            'orderby' => 'display_name'
            ) );
        $results = $users_query->get_results();
        if ($results) $users = array_merge($users, $results);
    endforeach;

    return $users;
}
$users_array = get_clients();
?>

<div class="container-wrap">
	
	<div class="container main-content">
		
		<div class="row">
						
	<?php
	if (!empty($users_array))
	{
		?>
<!--			<form id="account-type" action="#filter" method="post">
					<select name="account_type" id="account_type">
				<div id="account_type">
					<?php
						$account_selected = $_POST['account_type'];
						$account_type = array();
						$account_type['all'] = "All Account Types";
						$account_type['individual']  = "Individual";
						$account_type['startup']  = "Startup";
						$account_type['corporate']  = "Corporate";
						$account_type = array_map( 'trim', $account_type );
						$account_type = array_unique( $account_type );
						foreach ( $account_type as $id => $item ) {
					?>
						<option id="<?php echo $id; ?>" value="<?php echo esc_attr($item); ?>"<?php selected($account_selected , $item ); ?>><?php echo $item; ?></option>
					<div class="selectAccountType <?php if($_GET['account_type'] == esc_attr($item) || ($_GET['account_type'] == "" && $item == "All Account Types")) echo 'selected'; ?>"><a href="?account_type=<?php echo esc_attr($item); ?>"><?php echo esc_attr($item); ?></a></div>


					<?php
						}
					?>						
					</select>

				<br><br><INPUT TYPE = "Text" VALUE ="Search in Community" NAME = "filtername" size="35" onclick="this.value='';">			

				</div>
			</form>
			<br/>-->
			<?php
		echo '<div class="userList">';                        
		// loop trough each author
		foreach ($users_array as $user)
		{
			// get all the user's data
			$user_info = get_userdata($user->ID);
			if($_GET['account_type'] != "All Account Types" && !empty($_GET['account_type'])) {
				$account_filter =  $_GET['account_type'];
				if($account_filter != get_user_meta($user_info->ID, 'account_type', true)) {
					continue;
				}
			}

			if($_POST['filtername'] != NULL) {


				$filtername = $_POST['filtername'];
				$text = "a_" . $user_info->display_name;

				

				if (stripos(strtoupper($text), strtoupper($filtername)) != false) {
				}else{
					continue;
				}

			}
			if(USERPROFILE_OUTLINE == true) {
				switch(get_user_meta($user_info->ID, 'account_type', true)) {
					case "Startup":
					$ribbonColor = "green";
						break;
					case "Corporate":
					$ribbonColor = "orange";
						break;
					default:
						$ribbonColor = "blue";                            
				}

				echo '<div class="user withDecor"><div class="ribbon-wrapper"><div class="ribbon-'.$ribbonColor.'"></div></div>';                            
			}
			else if (USERPROFILE_CITY == true) {
				echo '<div class="user withDecor">';
				$city = get_user_meta($user_info->ID, 'current_location', true)."&nbsp;";
				echo '<div class = "city-wrapper"><div>'.$city.'</div></div>';
			}
			else {
				echo '<div class="user">';
			}
			if (function_exists('get_avatar')) { 
//                                if(LINKEDIN_ONLY == true) {
//                                    echo '<a href="'.$user_info->user_url.'" target="_blank">';                                    
//                                }
//                                else {
                                    echo '<a href="'.site_url().'/user-profile/?user_id='.$user_info->ID.'">';                                    
//                                }
				echo get_avatar( $user_info->user_email, 264 ); 
				echo "</a><br/>";
			}
//                        if(LINKEDIN_ONLY == true) {
//                            echo '<a href="'.$user_info->user_url.'" target="_blank">'.$user_info->display_name.'</a>';                            
//                            
//                        }
//                        else {
                            echo '<a href="'.site_url().'/user-profile/?user_id='.$user_info->ID.'">'.$user_info->display_name.'</a><br>';                            
//                        }
				$title = get_user_meta($user_info->ID, 'title', true);
				if(!empty($title)) {
					echo $title;
					echo "<br>";			
				}		
				$address = get_user_meta($user_info->ID, 'address', true);
				if(!empty($address)) {
					echo $address;
					echo "<br>";			
				}
				$twitter = get_user_meta($user_info->ID, 'twitter', true);
				if(!empty($twitter)) {
					echo '<b>Twitter:</b> '.$twitter;
					echo "<br>";			
				}			
//						$phone = get_user_meta($user_info->ID, 'phone', true);
//						if(!empty($phone)) {
//								echo "<br>";
//								echo '<b>Phone:</b> '.$phone;							
//						}
//						if(!empty($user_info->user_url)) {
//								echo "<br>";
//								echo '<a class="truncated" href="'.$user_info->user_url.'" target="_blank">'.$user_info->user_url.'</a>';				
//						}
			echo '</div>';
		}
		echo '</div><br/>';
		echo '<div style="clear:both;"></div>';
	} else {
		echo 'No users found';
	}
	?>
	
		</div><!--/row-->
		
	</div><!--/container-->
	
</div>
<?php get_footer(); ?>