<?php
# Database Configuration
define( 'DB_NAME', 'wp_nextbank_wpmu' );
define( 'DB_USER', 'root' );
define( 'DB_PASSWORD', '' );
define( 'DB_HOST', 'localhost' );
define( 'DB_HOST_SLAVE', 'localhost' );
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', 'utf8_unicode_ci');
$table_prefix = 'wp_';

# Security Salts, Keys, Etc
define('AUTH_KEY',         '|p`eduR q{{`m@<RSjEf,|p|])qVArOJ#5L#M^Cqt3La$PX|*0^VQt^syMF=XMLk');
define('SECURE_AUTH_KEY',  '2K#`LO&wTM{;&@k_Vm0-rkYa!s<)ZI/@n>b.S|*UO6+vW.M,Y+^~<4Zt{^b||z=!');
define('LOGGED_IN_KEY',    'ULlC#>H8PFDf:/a=Y$9,)V#>5{*S}fY^5Kj)9Zr>N|[+]BG=Vc|h%z(m|.n{UMJW');
define('NONCE_KEY',        '6_8zSDf;_~P-w%v|9nvO-|9 XXF24g+ow!U06F~CC4+9fMn*a==k|VSi/+5nSip-');
define('AUTH_SALT',        'c8-AQ39|y+vqtzD&{#BSoWDoQ0U<,kYZxEC>C!&R$QagiBi$M7rU%WB5das]-T(^');
define('SECURE_AUTH_SALT', 'y_&X;?cESqO4pi#2E*+v+yhiH2aY!vkiBx%S_U1i@{<$YwP)S7186+T;XeE0Q&eA');
define('LOGGED_IN_SALT',   'y||OJ8Gd8e^hB>3DJ8&0zLD#b$x6xkwZE+IWZvooQNvrF3mR+V9Q}ZT+1|u^}twh');
define('NONCE_SALT',       'z]]9(Tk@ju}6)x-OUh*e9_9ek=e^-E:`*XHzx2Jc9.P|72*DHw#2Rid+D8|R_G_~');


# Localized Language Stuff

define( 'WP_CACHE', TRUE );

define( 'WP_AUTO_UPDATE_CORE', false );

define( 'PWP_NAME', 'nextbank' );

define( 'FS_METHOD', 'direct' );

define( 'FS_CHMOD_DIR', 0775 );

define( 'FS_CHMOD_FILE', 0664 );

define( 'PWP_ROOT_DIR', '/nas/wp' );

define( 'WPE_APIKEY', 'd06af5e4de2aa9c9bf2931d95ab711f889a9582e' );

define( 'WPE_FOOTER_HTML', "" );

define( 'WPE_CLUSTER_ID', '1252' );

define( 'WPE_CLUSTER_TYPE', 'pod' );

define( 'WPE_ISP', true );

define( 'WPE_BPOD', false );

define( 'WPE_RO_FILESYSTEM', false );

define( 'WPE_LARGEFS_BUCKET', 'largefs.wpengine' );

define( 'WPE_CACHE_TYPE', 'generational' );

define( 'WPE_LBMASTER_IP', '66.175.209.46' );

define( 'WPE_CDN_DISABLE_ALLOWED', false );

define( 'DISALLOW_FILE_EDIT', FALSE );

define( 'DISALLOW_FILE_MODS', FALSE );

define( 'DISABLE_WP_CRON', false );

define( 'WPE_FORCE_SSL_LOGIN', false );

define( 'FORCE_SSL_LOGIN', false );

/*SSLSTART*/ if ( isset($_SERVER['HTTP_X_WPE_SSL']) && $_SERVER['HTTP_X_WPE_SSL'] ) $_SERVER['HTTPS'] = 'on'; /*SSLEND*/

define( 'WPE_EXTERNAL_URL', false );

define( 'WP_POST_REVISIONS', FALSE );

define( 'WPE_WHITELABEL', 'wpengine' );

define( 'WP_TURN_OFF_ADMIN_BAR', false );

define( 'WPE_BETA_TESTER', false );

umask(0002);

$wpe_cdn_uris=array ( );

$wpe_no_cdn_uris=array ( );

$wpe_content_regexs=array ( );

$wpe_all_domains=array ( 0 => 'nextbank.wpengine.com', );

$wpe_varnish_servers=array ( 0 => 'pod-1252', );

$wpe_special_ips=array ( 0 => '66.175.209.46', );

$wpe_ec_servers=array ( );

$wpe_largefs=array ( );

$wpe_netdna_domains=array ( );

$wpe_netdna_domains_secure=array ( );

$wpe_netdna_push_domains=array ( );

$wpe_domain_mappings=array ( );

$memcached_servers=array ( 'default' =>  array ( 0 => 'unix:///tmp/memcached.sock', ), );

define( 'WPE_SFTP_PORT', 22 );
define('WPLANG','');

# WP Engine ID


define('PWP_DOMAIN_CONFIG', 'nextbank.wpengine.com' );

# WP Engine Settings




define( 'WP_ALLOW_MULTISITE', true );
define( 'MULTISITE', true );
define( 'SUBDOMAIN_INSTALL', true );
$base = '/';
define( 'DOMAIN_CURRENT_SITE', 'localhost' );
define( 'PATH_CURRENT_SITE','/nextbank_wpmu' );
define( 'SITE_ID_CURRENT_SITE', 1 );
define( 'BLOG_ID_CURRENT_SITE', 1 );

echo ABSPATH;
# That's It. Pencils down
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
require_once(ABSPATH . 'wp-settings.php');

$_wpe_preamble_path = null; if(false){}
